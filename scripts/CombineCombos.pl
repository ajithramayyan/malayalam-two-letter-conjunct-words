#!/usr/bin/perl

use utf8;
use strict;
use autodie;
use warnings;
use warnings    qw< FATAL  utf8     >;
use open        qw< :std  :utf8     >;
use charnames   qw< :full >;
use feature     qw< unicode_strings >;
use File::Basename      qw< basename >;
use Carp                qw< carp croak confess cluck >;
use Encode              qw< encode decode >;
use Unicode::Normalize  qw< NFD NFC >;


while (<>) {
chomp;
sub  trim { my $s = shift; $s =~ s/^\s+|\s+$//g; return $s };
open(cf, ">>" . $_ . ".html") or die "Can't open file : $!";
print cf '<!DOCTYPE html>'."\n\t".'<html>'."\n\t\t".'<head>'."\n\t\t\t".'<link rel="stylesheet" type="text/css" href="malwords.css">'."\n\t\t".'</head>'."\n\t\t".'<body>'."\n";

open my $sf, '<' . $_ . "S.txt" or die "Can't open file $!";
read $sf, my $sft, -s $sf;
if (trim($sft) ne "")
    {
    print cf '<mark><div class="p">'.$_." ആദ്യത്തെ അക്ഷരത്തിന്റെ ഭാഗമാകുന്ന വാക്കുകൾ".'</div></mark>'."\n<p>\n";
    print cf $sft;
    print cf "\n</p>\n";
    }
else
    {
    print cf '<mark><div class="a">'.$_." ആദ്യത്തെ അക്ഷരത്തിന്റെ ഭാഗമാകുന്ന വാക്കുകൾ ഇല്ല".'</div></mark>'."\n";
    }
    
open my $mf, "<" . $_ . "M.txt" or die "Can't open file $!";
read $mf, my $mft, -s $mf;
if (trim($mft) ne "")
    {
    print cf '<mark><div class="p">'.$_." മദ്ധ്യത്തിലെ അക്ഷരങ്ങളുടെ ഭാഗമാകുന്ന വാക്കുകൾ".'</div></mark>'."\n<p>\n";
    print cf $mft;
    print cf "\n</p>\n";
    }
else
    {
    print cf '<mark><div class="a">'.$_." മദ്ധ്യത്തിലെ അക്ഷരങ്ങളുടെ ഭാഗമാകുന്ന വാക്കുകൾ ഇല്ല".'</div></mark>'."\n";
    }
    
open my $ef, '<' . $_ . "E.txt" or die "Can't open file $!";
read $ef, my $eft, -s $ef;
if (trim($eft) ne "")
    {
    print cf '<mark><div class="p">'.$_." അവസാനത്തെ അക്ഷരത്തിന്റെ ഭാഗമാകുന്ന വാക്കുകൾ".'</div></mark>'."\n<p>\n";
    print cf $eft;
    print cf "\n</p>\n";
    }
else
    {
    print cf '<mark><div class="a">'.$_." അവസാനത്തെ അക്ഷരത്തിന്റെ ഭാഗമാകുന്ന വാക്കുകൾ ഇല്ല".'</div></mark>'."\n";
    }
print cf "</body>\n</html>";
}
