library(tidyverse)
library(readxl)
cnt2 <- read_excel("Count2.xlsx")

cnt2 <- cnt2 %>% mutate(sCatA = case_when(
                                      str_detect(startCat, "conjunct|chillu") ~ "Malayalam",
                                      str_detect(startCat, "error|None") | is.na(startCat) ~ "Empty", 
                                      str_detect(startCat, "foreign") ~ "Others",
                                      TRUE ~ "?"),
                        mCatA = case_when(
                          str_detect(midCat, "conjunct|chillu") ~ "Malayalam",
                          str_detect(midCat, "error|None")  | is.na(midCat) ~ "Empty", 
                          str_detect(midCat, "foreign") ~ "Others",
                          TRUE ~ "?"),
                        eCatA = case_when(
                          str_detect(endCat, "conjunct|chillu") ~ "Malayalam",
                          str_detect(endCat, "error|None") | is.na(endCat) ~ "Empty", 
                          str_detect(endCat, "foreign") ~ "Others",
                          TRUE ~ "?"), 
                        catF =  case_when(
                          str_detect(sCatA, "Malayalam") | str_detect(mCatA, "Malayalam") | str_detect(eCatA, "Malayalam") ~ "Malayalam",
                          str_detect(sCatA, "Empty") & str_detect(mCatA, "Empty") & str_detect(eCatA, "Empty") ~ "Empty",
                          TRUE ~ "Others"))

write_tsv(cnt2, "Counts2Complete.txt")

sel_color <- c("conjunct" = "<mark class = co> ",
               "chillu" = "<mark class = ch> ",
               "foreign" = "<mark class = f> ",
               "error" = "<mark class = er> ",
               "None" = "<mark class = em> ")


cnt2 <- cnt2 %>% mutate( Total = Start + Middle + End,
                title = case_when(Total == 0 ~ paste0('There are no words with ', Conjunct),
                                  TRUE ~ paste0('Unique words in\nstarting syllable: ',Start,'\nMiddle syllables: ', Middle, '\nEnding syllable: ', End)),
                row_begin = case_when(Total == 0 ~ paste0('\n\t<td>\n\t\t<a href="words/none.html', 'class="no-underline"  title="', title ,'">') ,
                                      TRUE ~ paste0('\n\t<td>\n\t\t<a href="words/', Conjunct, '.html" class="no-underline"  title="', title ,'">')),
                sCell = paste0('\n\t\t', sel_color[startCat], "&nbsp;&#x25C1; </mark>"),
                mCell = paste0('\n\t\t',sel_color[midCat], Conjunct,' </mark>'),
                eCell = paste0('\n\t\t',sel_color[endCat], "&#x25B7;&nbsp; </mark>"),
                row_end = '\n\t</td>',
                startChar = str_sub(Conjunct,1,1),
                endChar = str_sub(Conjunct,-1,-1),
                tr = paste0(row_begin, sCell, mCell, eCell, row_end))

arrange(cnt2, Conjunct) %>% group_by(startChar) %>% 
  summarise(thr = paste0(tr, collapse = "")) %>% 
  mutate(rc = paste0('<tr>\n\t<th>',startChar,'</th>', thr, '\n</tr>'))%>% 
  select(rc) %>% write_tsv("c2index.html", quote_escape = "none")

cnt2 %>% filter(Total != 0) %>% select(Conjunct) %>% write_tsv("tomakehtml")
