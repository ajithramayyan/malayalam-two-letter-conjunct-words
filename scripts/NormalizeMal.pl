#!/usr/bin/perl

use utf8;
use strict;
use autodie;
use warnings;
use warnings    qw< FATAL  utf8     >;
use open        qw< :std  :utf8     >;
use charnames   qw< :full >;
use feature     qw< unicode_strings >;
use File::Basename      qw< basename >;
use Carp                qw< carp croak confess cluck >;
use Encode              qw< encode decode >;
use Unicode::Normalize  qw< NFD NFC >;


$^I = '.bak'; # create a backup copy

while (<>) {
s/<.*>//g;  # remove html tags between < and > in Wiki files
s/\h+/ /g;  # change consecutive horizontal spaces to single spaces
s/\R+/\n/g; # change consecutive vertical spaces to single newline
s/(?<!\[.\?!])\n/ /g; # convert newlines that does not follow a period to a single space
s/([.\?!])(?!\R) /$1\n/g; # add newline after all periods
s/^\h+//g; # remove space at start of line
s/ണ്‍/ൺ/g;  # change old chillu notation \u0D23\u0D4D\u200D to atomic notation \u0D7A
s/ല്‍/ൽ/g;  # change old chillu notation \u0D32\u0D4D\u200D to atomic notation \u0D7D
s/ര്‍/ർ/g;  # change old chillu notation \u0D30\u0D4D\u200D to atomic notation \u0D7C
s/ള്‍/ൾ/g; # change old chillu notation \u0D33\u0D4D\u200D to \u0D7E
s/ന്‍റ/ഩ്ഺ/g; # change \u0D28\u0D4D\u200D\u0D31 nta to phonetically correct version \u0D29\u0D4D\u0D3A
s/ൻ്റ/ഩ്ഺ/g;    # change \u0D7B\u0D4D\u0D31 nta to phonetically correct version u0D29\u0D4D\u0D3A
s/ന്റ/ഩ്ഺ/g;    # change \u0D28\u0D4D\u0D31 nta to phonetically correct version \u0D29\u0D4D\u0D3A
s/ന്‍/ൻ/g;  # change old chillu notation \u0D28\u0D4D\u200D to atomic  notation \u0D7B
s/റ്റ/ഺ്ഺ/g;    # change \u0D31\u0D4D\u0D31 tta to phonetically correct version \u0D3A\u0D4D\u0D3A
s/ൗ/ൌ/g;  # change  new \u0D57 au date sign to old au \u0D4C
s/([^ഗജഡദബഘഝഢധഭ])്ര/$1്റ/g;    # change ്ര to ്റ where appropriate
s/([^തനലസ])്ല/$1്ള/g;   # change  ്ല to ്ള where appropriat#e
s/[^\p{Block: Malayalam}\p{Block: Basic_Latin}\p{Block: General_Punctuation}\s]//g; # remove characters outside the specified unicode blocks
s/\p{Cf}//g; # remove characters outside the specified unicode blocks
#s/ /␣/g; replace space with open box
print; # print to the modified file
}
