library(tidyverse)
library(tidytext)
smc <- readLines("smcCorpus")
as_tibble(smc) %>% unnest_tokens(word,value) -> smcWords
smcWords %>% group_by(word) %>% summarise(count = n()) %>% arrange(desc(count)) %>%  write_tsv("smcWordsCount.txt", quote_escape = FALSE)
