#!/usr/bin/perl

use utf8;
use strict;
use autodie;
use warnings;
use warnings    qw< FATAL  utf8     >;
use open        qw< :std  :utf8     >;
use charnames   qw< :full >;
use feature     qw< unicode_strings >;
use File::Basename      qw< basename >;
use Carp                qw< carp croak confess cluck >;
use Encode              qw< encode decode >;
use Unicode::Normalize  qw< NFD NFC >;
 
open(CNTR, ">./Counts2.txt") or die "Can't open";
print CNTR "Conjunct\tStart\tMiddle\tEnd\n";
while (<>) {   # Read input from command-line
    chomp;
    my $search =  $_ ;
    my $cntS = 0;
    my $cntM = 0;
    my $cntE = 0;

    open(SLCT, ">./Conjunct2Words/".$search."S.txt") or die "Can't open file : $!"; # Create file handle to the file to write to.
    open(my $SMCW, "./smcWordsCount.txt") or die "Can't open smcWordsCount.txt : $!"; # Create file handle to the word file
    while (<$SMCW>) {           # Set $_ to each line of the file
        my $re = qr/^$search[^്ൺൻർൽൾം]+/dx;
        if (/$re/) {
            print SLCT $_;
            ++$cntS;
        }
    }

    open(SLCT, ">./Conjunct2Words/".$search."M.txt") or die "Can't open file : $!"; # Create file handle to the file to write to.
    open(my $SMCW, "./smcWordsCount.txt") or die "Can't open smcWordsCount.txt : $!"; # Create file handle to the word file
    while (<$SMCW>) {           # Set $_ to each line of the file
        my $re = qr/.*[^്ൺൻർൽൾം]+$search[^ാിീുൂൃൄെേൈൊോൌ്ൺൻർൽൾംഃ\s\t]+/dx; # shouldn't be part of a higher 
        if (/$re/) {
            print SLCT $_;
           ++$cntM;
        }
    }
    
    open(SLCT, ">./Conjunct2Words/".$search."E.txt") or die "Can't open file : $!"; # Create file handle to the file to write to.
    open(my $SMCW, "./smcWordsCount.txt") or die "Can't open smcWordsCount.txt : $!"; # Create file handle to the word file
    while (<$SMCW>) {  # Set $_ to each line of the file
        my $re = qr/[^്ൺൻർൽൾം]+$search [ൺൻർൽൾംഃാിീുൂൃൄെേൈൊോൌ്]*\t/dx; #  because end of the word is tab
        if (/$re/) {
            print SLCT $_;
           ++$cntE;
        }
    }


    print CNTR substr($search,0,-1) . "\t" . $cntS . "\t" . $cntM . "\t" . $cntE . "\n";
}
