#!/usr/bin/perl

use utf8;
use strict;
use autodie;
use warnings;
use warnings    qw< FATAL  utf8     >;
use open        qw< :std  :utf8     >;
use charnames   qw< :full >;
use feature     qw< unicode_strings >;
use File::Basename      qw< basename >;
use Carp                qw< carp croak confess cluck >;
use Encode              qw< encode decode >;
use Unicode::Normalize  qw< NFD NFC >;


$^I = '.bak'; # create a backup copy

while (<>) {
s/([^ഗജഡദബഘഝഢധഭ])്റ/$1്ര/g; # change back ്റ to ്ര where appropriate
s/([^തനലസ])്ള/$1്ല/g; # change back ്ള to ്ല where appropriat#e
s/ഩ്ഺ/ന്‍റ/g; # change back phonetically correct nta \u0D29\u0D4D\u0D3A to \u0D28\u0D4D\u200D\u0D31 
s/ഺ്ഺ/റ്റ/g; # change back to phonetically correct tta \u0D3A\u0D4D\u0D3A to \u0D31\u0D4D\u0D31  
print; # print to the modified file
}
